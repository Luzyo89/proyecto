﻿using DAL.Data;
using DAL.Services;
using Microsoft.EntityFrameworkCore;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace XUnitApiTest
{
    public class ClienteServicesTest
    {
        private readonly WebApiContext context;

        public ClienteServicesTest()
        {
            //Se debe instalar el paquete Microsoft.EntityFrameworkCore.InMemory
            var options = new DbContextOptionsBuilder<WebApiContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;
            context = new WebApiContext(options);
            context.Database.EnsureCreated();

            var usuarios = new List<User>()
            {
                new User(){Id=1, Name="Uno"},
                new User(){Id=2, Name="Dos"},
                new User(){Id=3, Name="Tres"},
                new User(){Id=4, Name="Cuatro"},
                new User(){Id=5, Name="Cinco"}
            };

            context.AddRange(usuarios);
            context.SaveChanges(); 
        }

        [Fact]
         async Task GetUsersRetornaTodosLosUsuariosTest()
        {
            //Arrange
            var clienteServices = new ClienteServices(context);
            //Act
            var AllUsers = await clienteServices.GetUsers();
            //Assert
            Assert.Equal(5,AllUsers.Count());
        }
        [Fact]
        async Task GetUserConIdValidoRetornaUsuarioTest()
        {
            //Arrange
            var clienteService = new ClienteServices(context);
            //Act
            var user = await clienteService.GetUser(5);
            //Assert
            Assert.NotNull(user);
        }
        [Fact]
        async Task GetUserConIdInValidoRetornaFalseTest()
        {
            //Arrange
            var clienteService = new ClienteServices(context);
            //Act
            var user = await clienteService.GetUser(6);
            //Assert
            Assert.Null(user);
        }
        [Fact]
        async Task PostUserUsuarioValidoRetornaTrueTest()
        {
            //Arrange
            var clienteService = new ClienteServices(context);
            var user = new User() { Name="Usuario X" };
            //Act
            var service = await clienteService.PostUser(user);
            //Assert
            Assert.True(service);
        }
        [Fact]
        async Task PostUserUsuarioNuloRetornaFalseTest()
        {
            //Arrange
            var clienteService = new ClienteServices(context);
            User user = null;
            //Act
            var service = await clienteService.PostUser(user);
            //Assert
            Assert.False(service);
        }
        [Fact]
        async Task PutUserInfoValidaRetornaTrueTest()
        {
            //Arrange
            var clienteService = new ClienteServices(context);
            var user = new User() { Id = 1, Name = "1" };
            //Act
            var service = await clienteService.PutUser(user);
            //Assert
            Assert.True(service);
        }
        [Fact]
        async Task PutUserUsuarioInexistenteRetornaFalseTest()
        {
            //Arrange
            var clienteService = new ClienteServices(context);
            var user = new User() { Id = 0, Name = "Usuario Cero" };
            //Act
            var service = await clienteService.PutUser(user);
            //Assert
            Assert.False(service);
        }
        [Fact]
        async Task DeleteUserUsuarioExistenteRetornaTrueTest()
        {
            //Arrange
            var clienteService = new ClienteServices(context);
            //Act
            var service = await clienteService.DeleteUser(1);
            //Assert
            Assert.True(service);
        }
        [Fact]
        async Task DeleteUserUsuarioInexistenteRetornaFalseTest()
        {
            //Arrange
            var clienteService = new ClienteServices(context);
            //Act
            var service = await clienteService.DeleteUser(0);
            //Assert
            Assert.False(service);
        }
    }
}
