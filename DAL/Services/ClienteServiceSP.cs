﻿using DAL.Data;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Services
{
    public class ClienteServiceSP
    {
        private readonly WebApiContext _context;

        public ClienteServiceSP(WebApiContext context)
        {
            _context = context;
        }


        public async Task<IEnumerable<User>> GetUsers()
        {
            //En NetCore 3.0+ se usa el método FromSqlRaw, para las versiones
            //inferiores hay que utilizar FromSql.
            //Elegimos este método porque necesitamos valores de retorno.
            //Solo necesitamos pasar el nombre de nuestro stored procedure
            return await _context.Users.FromSqlRaw("UsersGetAll").ToListAsync(); ;
        }


        public async Task<User> GetUser(int id)
        {
            //En esta ocasion aparte del nombre del stored procedure debemos añadir 
            //el parametro en el string, y sobrecargar el método con un sqlparameter
            //que define el mismo nombre que recibe el SP en la base.
            var user = await _context.Users
            .FromSqlRaw("UsersGetById @Id", new SqlParameter("@Id", id)).ToListAsync();
                
            //Por defecto FromSqlRaw devuelve un IEnumerable, en este caso será una lista
            //de longitud uno por tanto también es válido retornar user[0].
            return user.FirstOrDefault();
        }


        public async Task<bool> PutUser(User user)
        {

            if (await GetUser(user.Id) == null)
            {
                return false;
            }

            try
            {
                //Dado que no necesitamos un retorno utilizaremos ExecuteSqlRawAsync
                //para netcore 3.0+ y ExecuteSqlCommand para versiones inferiores
                //agregamos cada parametro (sepadados por coma) al string del nombre de nuestro stored 
                //procedured, y sobrecargamos con tantos sqlparameters como parametros 
                //reciba nuestro procedimiento, cuidar que el nombre del parametro a recibir 
                //sea el mismo que el enviado.
                await _context.Database.ExecuteSqlRawAsync("UsersEdit @Id, @Name",
                new SqlParameter("@Id", user.Id),
                new SqlParameter("@Name", user.Name)
                );
                return true;
            }
            catch (DbUpdateConcurrencyException)
            {
                return false;
            }


        }

        public async Task<bool> PostUser(User user)
        {
            try
            {
                //Acá tampoco necesitamos recibir un retorno
                 await _context.Database.ExecuteSqlRawAsync("UsersCreate @Name",
                 new SqlParameter("@Name",user.Name));
                 return true;
            }
            catch (Exception e)
            {
                return false;
            }
         
        }


        public async Task<bool> DeleteUser(int id)
        {
           
            if (await GetUser(id) == null)
            {
                return false;
            }
            //Acá tampoco necesitamos recibir un retorno
            await _context.Database.ExecuteSqlRawAsync("UsersDelete @Id", new SqlParameter("@Id",id));

            return true;
        }

    }
}
