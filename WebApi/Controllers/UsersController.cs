﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models.Models;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        //private readonly ClienteServices _clienteService;
        private readonly ClienteServiceSP _clienteServiceSP;

        public UsersController(ClienteServices clienteService, ClienteServiceSP clienteServiceSP)
        {
            //_clienteService = clienteService;
            _clienteServiceSP = clienteServiceSP;
        }

       // GET: api/Users
       [HttpGet]
        public async Task<ActionResult<IEnumerable<User>>> GetUser()
        {
            //return  Ok(await _clienteService.GetUsers());
            return  Ok(await _clienteServiceSP.GetUsers());
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<ActionResult<User>> GetUser(int id)
        {
            var user = await _clienteServiceSP.GetUser(id);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        // PUT: api/Users/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser(int id, User user)
        {
            if (id != user.Id)
            {
                return NoContent();
            }
            if (ModelState.IsValid)
            {
              bool IsUpdate = await _clienteServiceSP.PutUser(user);
                if (IsUpdate)
                {
                    return Ok();
                }
                    return BadRequest();
            }

            return BadRequest(user);
        }

        // POST: api/Users
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<User>> PostUser(User user)
        {
            if (ModelState.IsValid)
            {
                if (await _clienteServiceSP.PostUser(user))
                {
                    return CreatedAtAction("GetUser", new { id = user.Id }, user);
                }
                return BadRequest();
            }

            return BadRequest(user);

            
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<User>> DeleteUser(int id)
        {
            var Exist = await _clienteServiceSP.GetUser(id);

            if (Exist==null)
            {
                return NotFound();
            }

            if (await _clienteServiceSP.DeleteUser(id))
            {
                return Ok();
            }
            
            return NoContent();
        }
      
    }
}
