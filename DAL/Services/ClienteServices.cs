﻿using DAL.Data;
using Microsoft.EntityFrameworkCore;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Services
{
    //Si se nos permite usar el asistente podemos adaptar un servicio como este.
    public class ClienteServices
    {
        private readonly WebApiContext _context;

        public ClienteServices(WebApiContext context)
        {
            _context = context;
        }

    
        public async Task<IEnumerable<User>> GetUsers()
        {
            return await _context.Users.ToListAsync();
        }

        
        public async Task<User> GetUser(int id)
        {
            var user = await _context.Users.FindAsync(id);

            return user;
        }

     
        public async Task<bool> PutUser(User user)
        {
            //con esta consulta nuestro context queda a la escucha de
            //los cambios de usuarioExistente y necesitariamos el método AsNoTracking 
            //para luego guardar (user) directamente en la base
            var usuarioExistente = await _context.Users.FindAsync(user.Id);
            if (usuarioExistente== null)
            {
                return false;
            }
            //así que mejor igualamos todas las propiedades usuarioExistente a user
            //la razón de esto es que usaremos una base en memoria para 
            //la prueba unitaria así que de está forma logramos que funcionen ambas cosas.
            usuarioExistente.Name = user.Name;
            try
            {
                _context.Update(usuarioExistente);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!await UserExists(user.Id))
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }

           
        }

        public async Task<bool> PostUser(User user)
        {
            try
            {
                _context.Users.Add(user);
                await _context.SaveChangesAsync();
                return true;
            }


            catch (Exception)
            {
                return false;
            }

        }
        public async Task<bool> DeleteUser(int id)
        {
            var user = await _context.Users.FindAsync(id);
            if (user == null)
            {
                return false;
            }

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> UserExists(int id)
        {
            return await _context.Users.AnyAsync(e => e.Id == id);
        }

    }
}
